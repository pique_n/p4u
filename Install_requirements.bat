﻿@echo off

rem Ermittle und zeige den aktuellen Pfad an
set "scriptPath=%~dp0"
echo Der aktuelle Pfad ist: %scriptPath%

rem Überprüfe, ob Python installiert ist
python --version > nul 2>&1
if %errorlevel% neq 0 (
    msg * Python ist nicht installiert. Bitte installiere Python und führe das Skript erneut aus.
    exit /b
)

rem Überprüfe, ob pip installiert ist
pip --version > nul 2>&1
if %errorlevel% neq 0 (
    msg * pip ist nicht installiert. Bitte installiere pip und führe das Skript erneut aus.
    exit /b
)

rem Installiere Abhängigkeiten mit pip
pip install -r .\Script\requirements.txt

rem Überprüfe, ob die Installation erfolgreich war
if %errorlevel% neq 0 (
    msg * Fehler: Die pip-Installation ist fehlgeschlagen. Bitte überprüfe die obige Fehlermeldung.
    exit /b
)

msg * Pip-Installation erfolgreich.

rem Erstelle eine Verknüpfung zu P4U-Converter.bat
set "shortcutTarget=%scriptPath%Script/P4U-Converter.bat"
set "shortcutName=%scriptPath%P4U-Converter.lnk"

echo Set oWS = WScript.CreateObject("WScript.Shell") > CreateShortcut.vbs
echo sLinkFile = "%shortcutName%" >> CreateShortcut.vbs
echo Set oLink = oWS.CreateShortcut(sLinkFile) >> CreateShortcut.vbs
echo oLink.TargetPath = "%shortcutTarget%" >> CreateShortcut.vbs
echo oLink.Save >> CreateShortcut.vbs
cscript /nologo CreateShortcut.vbs
del CreateShortcut.vbs

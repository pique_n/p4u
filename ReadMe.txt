
#################################################################################
#
# P4U Script for converting Baskets from Digikey / Mouser to P4U format
# Created by Jonas Lingua & Noah Piqué
# Date: 30.11.2023
# Version 1.4
#
################################################################################

## New:
- Use via website

## Infos:
- python3 script

# Instruction Windows:
- Install Python 3 via Softwarekiosk
- Execute"Install_requirements.bat" # You only have to run this command once!
- Execute "P4U-Converter" # This link should be displayed when the requirements have been successfully installed. Drag this link to the place you want.

# Instruction Linux/MacOS:
- Install Python 3 via Softwarekiosk
cd Script
pip install -r requirements.txt # You only have to run this command once!
python app.py # for web-based upload
or
python main.py [mouser/digikey] [filename] [fasttrack] # for console-use
  evt. python3, depending on the installation environment

# Console changes
If you still want to use the console, please put your xls/csv files in the "uploads" folder.
Your new carts will be saved in the "exports" folder.

# Distributor informations
- mouser -> xls file in euros!
- digikey -> csv file in chf!
- please dont change this file

# using the app.py script for any distributor:
- go to distributor and create cart
- then set currency to euros/chf (See "Distributor informations")
- download cart xls/csv (See "Distributor informations")
- excute the link/command
- upload the file in the webinterface
- click on download
- now you can upload this file to p4u

# using the main.py script for any distributor:
- go to distributor and create cart
- then set currency to euros/chf (See "Distributor informations")
- download cart xls/csv (See "Distributor informations")
- put the file in the same folder as the main.py script
- execute the command from "Instruction Windows/Linux/MacOS":
python main.py mouser name_of_my_basket.xls
- OR execute this command for fasttrack order:
python main.py mouser name_of_my_basket.xls fasttrack
- outputs file -> Warenkorb_distributor.xlsx
- now you can upload this file to p4u


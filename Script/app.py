
#################################################################################
#
# P4U Script for converting Baskets from Digikey / Mouser to P4U format
# Created by Jonas Lingua & Noah Piqué
# Date: 30.11.2023
# Version 1.4
#
################################################################################

# Importieren der erforderlichen Module
from flask import Flask, request, render_template, send_file
import os
import main
import csv
import webbrowser

url = "http://127.0.0.1:5000"
webbrowser.open_new_tab(url)


# Erstellen einer Flask-App-Instanz
app = Flask(__name__)

# Überprüfe, ob der Ausgabeordner existiert, und erstelle ihn, falls nicht
if not os.path.exists('exports'):
    os.makedirs('exports')
# Überprüfe, ob der Eingabeordner existiert, und erstelle ihn, falls nicht
if not os.path.exists('uploads'):
    os.makedirs('uploads')
# Festlegen des Upload-Verzeichnisses
UPLOAD_FOLDER = 'uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Globale Variable zur Speicherung des Slider-Status
fasttrack_status = True

# Routen-Handler für die Startseite
@app.route('/')
def index():
    return render_template('index.html')


# Routen-Handler für das Aktualisieren des Slider-Status
@app.route('/update_slider_status', methods=['POST'])
def update_slider_status():
    global fasttrack_status
    data = request.get_json()
    new_status = data.get('fasttrack_status')
    fasttrack_status = new_status
    return 'Slider-Status aktualisiert'

# Routen-Handler für das Hochladen von Dateien
@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' in request.files:
        file = request.files['file']
        if file:
            # Speichern der hochgeladenen Datei im Upload-Verzeichnis
            uploaded_file_path = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
            file.save(uploaded_file_path)
            dist = distributor(uploaded_file_path)
            global new_file
            new_file = 'Warenkorb_' + dist + '.xlsx'
            web_in = ['main.py', dist, file.filename]
            if fasttrack_status:
                web_in.append('fasttrack')
            else:
                web_in = web_in[0:3]
            main.p4u(web_in)
            clearFolder("uploads")
            return main.printout
    return 'Error uploading the file. See console'

# Routen-Handler für das Herunterladen der output-Datei
@app.route('/download_output')
def download_output_file():
    output_file_path = os.path.join('exports', new_file)
    return send_file(output_file_path, as_attachment=True)

def clearFolder(path):
    try:
        dateien = os.listdir(path)
        for datei in dateien:
            datei_pfad = os.path.join(path, datei)
            if os.path.isfile(datei_pfad):
                os.remove(datei_pfad)  # Lösche die Datei
            elif os.path.isdir(datei_pfad):
                os.rmdir(datei_pfad)  # Lösche den Ordner
    except Exception as e:
        print(f"Error during deleting the folder: {str(e)}")

def distributor(filename):
    if filename[-4:] == '.csv':
        try:
            with open(filename, 'r') as CSV:
                read_csv = csv.reader(CSV, delimiter=",")
                read_csv = list(read_csv)
                if read_csv[1][2][-3:] == '-ND':
                    return 'digikey'
                else:
                    return 'not found'
        except:
            print("Error: could not open the file")
            exit(1)
    else:
        return 'mouser'

# Starten der Flask-Anwendung, wenn die Datei direkt ausgeführt wird
if __name__ == '__main__':
    app.run(debug=False)
    clearFolder('exports')

@echo off
chcp 65001 >nul
setlocal enabledelayedexpansion
set "script_directory=%~dp0"

:: �berpr�fe, ob 'python3' oder 'python' verf�gbar ist
set python_cmd=
for %%P in (python3 python) do (
    %%P --version >nul 2>&1
    if !errorlevel! equ 0 (
        set python_cmd=%%P
        goto :found_python
    )
)

:found_python
if "%python_cmd%"=="" (
  echo [FEHLER] Kein funktionierender Python-Interpreter gefunden.
  exit /b 1
)

echo [INFO] Python-Interpreter gefunden: %python_cmd%

cd /d "%script_directory%"

:: Starte die Python-Anwendung und zeige die Ausgabe in der Konsole
echo [INFO] Starte die Python-Anwendung...
%python_cmd% "app.py"

exit /b 0
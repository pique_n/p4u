#################################################################################
#
# P4U Script for converting Baskets from Digikey / Mouser to P4U format
# Created by Jonas Lingua & Noah Piqué
# Date: 30.11.2023
# Version 1.4
#
################################################################################


from xlrd import open_workbook
from openpyxl import load_workbook
import sys
import csv

printout = ""
def prints(string):
    global printout
    printout += str(string) + "<br />"
    print(string)

# converting mouser xls file to array
def load_mouser(file_mouser, fast=False):
    try:
        wb = open_workbook(file_mouser)
    except:
        prints("Error(Mouser): could not open the file")
        return 0

    prints('Reading File Succeded')

    sh = wb.sheet_by_index(0)

    col = sh.col_values(0)
    offset_mouser = col.index(1)
    if offset_mouser == -1:
        prints('Error(Mouser): did not find a part')
        return 0

    nparts = col.index('', offset_mouser) - offset_mouser
    if nparts <= -1:
        prints('Error(Mouser): did not the end of the file')
        return 0

    vertrag = 'Mouser Electronics, Elsenheimerstrasse 11, 80687, München, DE (EUR)'
    if fast:
        vertrag = 'Mouser Electronics (EUR) (Fasttrack)'
    part_mouser = ['', '', vertrag, 'Mouser Electronics', '', '', '', '', '', 820, 0, 'Stück', '', '', '', '', '', '8.1', 0, 'EUR']
    parts_mouser = []

    for part in range(nparts):
        parts_mouser.append(part_mouser.copy())

    for x, y in enumerate(range(offset_mouser, offset_mouser + nparts)):
        parts_old = sh.row_values(y, 0, 11)
        parts_mouser[x][0] = str(int(parts_old[0]))
        parts_mouser[x][1] = parts_old[1]
        parts_mouser[x][4] = parts_old[3]
        parts_mouser[x][5] = parts_old[2]
        parts_mouser[x][6] = parts_old[3] + " " + parts_old[2]
        parts_mouser[x][7] = parts_old[5]
        parts_mouser[x][10] = int(parts_old[8])
        w = parts_old[9][-1]
        if w != '€':
            prints('Error: please change mouser currency to euro and retry')
            return 0
        try:
            p = float(parts_old[9][0:-2].replace(',', '.'))
        except ValueError:
            prints("Error: mouser price is not in right format")
            return 0
        parts_mouser[x][18] = p

    #prints(parts_mouser)

    return parts_mouser


# converting digikey csv file to array
def load_digikey(file_digikey, fast=False):
    try:
        with open(file_digikey, 'r') as CSV:
            read_csv = csv.reader(CSV, delimiter=",")
            read_csv = list(read_csv)
    except:
        prints("Error: could not open the file")
        return 0

    prints('Reading File Succeded')

    offset_digikey = -1
    for i, row in enumerate(read_csv):
        if (row[0]) == '1':
            offset_digikey = i
            break

    if offset_digikey <= -1:
        prints('Error(Digikey): did not the start of the file')
        return 0

    nparts = len(read_csv) - offset_digikey

    vertrag = 'Digi-Key Corporation, Brooks Avenue South 701, 56701, Thief River Falls, US (CHF)'
    if fast:
        vertrag = 'Digi-Key Corporation (CHF) (Fasttrack)'
    # Index,"Menge","Teilenummer","Hersteller-Teilenummer","Beschreibung","Kundenreferenz","Verfügbar","Lieferrückstände","Stückpreis","Gesamtpreis EUR"
    # Position	Artikelnummer	Vertrag 	Lieferant	Hersteller	Herstellernummer	Kurztext	Langtext	Materialnummer	Standardwarengruppe	Menge	Einheiten ppppp	MwSt-Satz	Preis	Währung
    part_digikey = ['', '', vertrag, 'Digi-Key Corporation', '', '', '', '', '', 820, 0, 'Stück', '', '', '', '', '', '8.1', 0, 'CHF']
    parts_digikey = []

    for part in range(nparts):
        parts_digikey.append(part_digikey.copy())

    for x, y in enumerate(range(offset_digikey, offset_digikey + nparts)):
        parts_old = read_csv[y]
        parts_digikey[x][0] = str(int(parts_old[0]))
        parts_digikey[x][1] = parts_old[2]
        parts_digikey[x][5] = parts_old[3]
        parts_digikey[x][6] = parts_old[3]
        parts_digikey[x][7] = parts_old[4]
        parts_digikey[x][10] = int(parts_old[1])
        w = parts_old[9][-3:]
        if w != 'Fr.':
            prints('Error: please change digikey currency to chf and retry')
            return 0
        try:
            p = float(parts_old[8].replace(',', '.'))
        except ValueError:
            prints("Error: digikey price is not in right format")
            return 0
        parts_digikey[x][18] = p

    # prints(parts_digikey)

    return parts_digikey

load_functions = {'mouser': load_mouser, 'digikey': load_digikey}
fasttrack = ['mouser', 'digikey']

def p4u(args):
    global printout
    printout = ""

    if len(args) <= 2:
        prints("Error: please specify a type(mouser/digikey) and a file")
        return 0
    else:
        file_type = args[1]
        file = "uploads/" + args[2]
        fast = ''
        if len(args) == 4:
            fast = args[3]

    parts = []

    if file_type in load_functions.keys():
        prints("Found load function for " + file_type)
        try:
            if fast == 'fasttrack' and file_type in fasttrack:
                prints('Fasttrack Order found')
                parts.extend(load_functions[file_type](file, True))
            else:
                prints('No Fasttrack')
                parts.extend(load_functions[file_type](file))
        except:
            prints("Error: could not open the file")
            return 0
    else:
        prints("Error: wrong type of file(mouser/digikey)")
        return 0

    if not parts:
        prints('Error: no data recieved (empty files?)(wrong filename?)')
        return 0

    new_file = './Warenkorb_' + file_type + '.xlsx'
    prints('Writing to new File: ' + new_file)

    try:
        wb_neu = load_workbook('./templates/Warenkorb.xlsx')
    except:
        prints("Error: template Warenkorb.xlsx is missing, please put it back in the templates folder")
        return 0

    sh_neu = wb_neu.active

    offset = 0

    for i, row in enumerate(sh_neu.values):
        if (row[0]) == 'Position':
            offset = i + 1

    for i in range(len(parts)):
        for j in range(len(parts[i])):
            sh_neu.cell(offset + i + 1, j + 1, parts[i][j])

    wb_neu.save('./exports/' + new_file)

    prints('Finished')
    return new_file

if __name__ == '__main__':

    if(not p4u(sys.argv)):
        print(printout)
        sys.exit(1)
    print("Done")
    #print(printout)